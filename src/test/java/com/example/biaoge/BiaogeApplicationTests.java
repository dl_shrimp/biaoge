package com.example.biaoge;

import com.example.biaoge.school.mapper.AccountMapper;
import com.example.biaoge.school.mapper.CoursesMapper;
import com.example.biaoge.school.util.Account;
import com.example.biaoge.school.util.Courses;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

@SpringBootTest
class BiaogeApplicationTests {
    @Autowired
    private CoursesMapper coursesMapper;
    @Autowired
    private AccountMapper accountMapper;

    @Test
    void contextLoads() {

//        Courses courses = new Courses();
//        Courses byCourses = coursesMapper.findByCourses(courses);
//        System.out.println(byCourses);
//
//        List<Kecheng> kechengList =kechengMapper.findAllKecheng();
//        for (Object kecheng : kechengList) {
//                        System.out.println(kecheng);
//        }
//        System.out.println(kechengList);

//       coursesMapper.save(courses);
//
//        List<Courses> allCourses = coursesMapper.findAllCourses();
//        System.out.println(allCourses);

        Account account = new Account();
        account.setMoney("16.00元");
        account.setThing("今天点了一个大盘鸡花了16元");
        Timestamp timestamp = new Timestamp(new Date().getTime());
        account.setData(timestamp);
//        System.out.println(timestamp);
        accountMapper.addAccount(account);
        List<Account> allAccount = accountMapper.findAllAccount();
        System.out.println(allAccount);

    }

}
