package com.example.biaoge;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BiaogeApplication {

    public static void main(String[] args) {
        SpringApplication.run(BiaogeApplication.class, args);
    }

}
