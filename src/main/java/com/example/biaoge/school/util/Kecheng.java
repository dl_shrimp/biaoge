package com.example.biaoge.school.util;

import lombok.Data;

@Data
public class Kecheng {
    private Integer cid;
    private String cname;
    private Integer week;
    private Integer jieci;
    private String  banji;
    private String address;
    private String tid;
}
