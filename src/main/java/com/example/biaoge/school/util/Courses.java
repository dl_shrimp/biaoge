package com.example.biaoge.school.util;

import lombok.Data;

@Data
public class Courses {
    private Integer id;
    private String day;
    private String one;
    private String two;
    private String three;
    private String four;
    private String five;
    private String six;
    private String seven;
    private String eight;

}
