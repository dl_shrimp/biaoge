package com.example.biaoge.school.util;

import lombok.Data;

import java.sql.Timestamp;
@Data
public class Account {
    private Integer accountId;
    private String thing;
    private Timestamp data;
    private String money;
}
