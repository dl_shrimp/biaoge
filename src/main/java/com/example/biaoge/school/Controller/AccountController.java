package com.example.biaoge.school.Controller;

import com.example.biaoge.school.mapper.AccountMapper;
import com.example.biaoge.school.util.Account;
import com.example.biaoge.school.util.Courses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
@ResponseBody
@RequestMapping("/account")
public class AccountController {
    @Autowired
    private AccountMapper accountMapper;
    @RequestMapping(value = "/findAllAccount", method = RequestMethod.GET)
    public List<Account> findAllAccount(Model model){
        List<Account> accountList=accountMapper.findAllAccount();
        model.addAttribute("accountList",accountList);
        return accountList;
    }
}
