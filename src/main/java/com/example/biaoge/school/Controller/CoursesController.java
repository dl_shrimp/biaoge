package com.example.biaoge.school.Controller;

import com.example.biaoge.school.service.CoursesService;
import com.example.biaoge.school.util.Courses;
import com.example.biaoge.school.util.Kecheng;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
@ResponseBody
public class CoursesController {
    @Autowired
    private CoursesService coursesService;
    @RequestMapping(value = "find", method = RequestMethod.GET)
    public Courses findOne(Courses courses, Model model){
        System.out.println(courses);
        model.addAttribute("courses",courses);
        return courses;
    }
    //查看所有课程表信息
    @RequestMapping(value = "findAllCourses", method = RequestMethod.GET)
    public List<Courses> findAllCourses(Model model){
        List<Courses> coursesList= coursesService.findAllCourses();
        System.out.println(coursesList);
        model.addAttribute("coursesList",coursesList);
        return coursesList;
    }



}
