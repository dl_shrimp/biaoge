package com.example.biaoge.school.service;

import com.example.biaoge.school.util.Courses;

import java.util.List;

public interface CoursesService {
    Courses findByCourses(Courses courses) ;

    List<Courses> findAllCourses();
}
