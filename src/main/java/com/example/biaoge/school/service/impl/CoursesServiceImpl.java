package com.example.biaoge.school.service.impl;

import com.example.biaoge.school.mapper.CoursesMapper;
import com.example.biaoge.school.service.CoursesService;
import com.example.biaoge.school.util.Courses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CoursesServiceImpl implements CoursesService {
    @Autowired
    private CoursesMapper coursesMapper;

    @Override
    public Courses findByCourses(Courses courses) {
        return coursesMapper.findByCourses(courses);
    }

    @Override
    public List<Courses> findAllCourses() {
        return coursesMapper.findAllCourses();
    }

}
