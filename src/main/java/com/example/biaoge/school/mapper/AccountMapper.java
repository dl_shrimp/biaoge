package com.example.biaoge.school.mapper;

import com.example.biaoge.school.util.Account;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
@Mapper
public interface AccountMapper {
    //所有查询账单
    List<Account>  findAllAccount();
    //增加账单
    int addAccount(Account account);


}
