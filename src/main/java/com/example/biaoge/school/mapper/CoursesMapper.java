package com.example.biaoge.school.mapper;

import com.example.biaoge.school.util.Courses;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface CoursesMapper {
    Courses findByCourses(Courses courses);

    void save(Courses courses);

    List<Courses> findAllCourses();
}
